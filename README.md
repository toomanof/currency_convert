##Test


Write a REST web service for currency conversion.
Exchange rates might be taken from free sources (e.g. ​https://openexchangerates.org/​) and should be updated once a day. The rates should be stored in a database.
User interface design is not important and up to you.
Currencies:
● Czech koruna
● Euro
● Polish zloty
● US dollar
The application should be tested as well. Code coverage is important. The project should be uploaded to GitHub/Bitbucket.

---
The task is completed in the programming language [Python 3.8](https://www.python.org/downloads/release/python-380/),
using [Django REST framework 3.12.1](https://www.django-rest-framework.org/),
[Celery5.0.0](https://docs.celeryproject.org/en/stable/),
[Redis](https://redis.io/)

---
## Installation and run:

1. git clone https://toomanof@bitbucket.org/toomanof/currency_convert.git
2. cd currency_convert/converter_curr
3. instalation Docker and Docker-compose
4. docker-compose build
5. docker-compose up

url application: 127.0.0.1:8000
url API documentations: 127.0.0.1:/api/v1
url latest rates from database: 127.0.0.1:/api/latest
url latest rates from European Central Bank: 127.0.0.1:/api/ecb/latest
url saves the current exchange rates to the database: 127.0.0.1:/api/ecb/save 

Testing occurs when the image is launched
