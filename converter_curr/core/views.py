from rest_framework.views import APIView

from .services import ResponseRates

class SaveRatesView(APIView):

    def get(self, request):
        """
        Saves the current exchange rates to the database.
        Returns exchange rates recorded at now from European Central Bank.
        Base currency: 'USD'
        Сurrencies: 'CZK', 'PLN', 'USD', 'EUR'
        """
        return ResponseRates().save_to_base_current_rates()


class ECBRatesView(APIView):

    def get(self, request):
        """
        Returns exchange rates recorded at 0:00 GMT from data from
        European Central Bank.

        Get request parameters:
        base - the main one
        currencies - currencies whose rates must be returned
        """
        currencies = ['CZK', 'PLN', 'USD', 'EUR']
        params = request.GET
        base = params['base'] if 'base' in params else 'RUB'
        if 'currencies' in params:
            currencies = params['currencies'].split(',')
        return ResponseRates().request_current_rates_ecb(base, *currencies)

class DBRatesView(APIView):

    def get(self, request):
        """
        Returns exchange rates recorded at 0:00 GMT from data from
        Database.
        """
        return ResponseRates().request_current_rates_db()