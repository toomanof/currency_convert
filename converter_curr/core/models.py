from django.db import models


class Rate(models.Model):
    base_curr = models.CharField('Base currency', max_length=5)
    curr = models.CharField('Currency', max_length=5)
    value = models.FloatField('Currency rate value')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)