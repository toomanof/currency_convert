from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status


client = Client()

class RatesTestCase(TestCase):
    def setUp(self):
        print('save to database latest rates ecb:')
        client.get(reverse('api_core:save_latest_rates_ecb'))

    def test_get_latest_rates_in_ecb(self):
        print('Test latest rates ecb:')
        response = client.get(reverse('api_core:latest_rates_ecb'))
        print('Response: ', response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_latest_rates_in_db(self):
        print('Test latest rates ecb:')
        response = client.get(reverse('api_core:save_latest_rates_ecb'))
        print('Response: ', response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
