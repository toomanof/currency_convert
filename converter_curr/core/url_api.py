from django.urls import path
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from .views import SaveRatesView, ECBRatesView, DBRatesView


app_name = 'api_core'

schema_view = get_schema_view(
   openapi.Info(
      title="API Converter currencies",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
        path('latest', DBRatesView.as_view(), name='latest_rates'),
        path('ecb/latest', ECBRatesView.as_view(), name='latest_rates_ecb'),
        path('ecb/save', SaveRatesView.as_view(),
             name='save_latest_rates_ecb'),
        path('swagger.json', schema_view.without_ui(cache_timeout=0),
             name='schema-json'),
        path('v1', schema_view.with_ui('swagger', cache_timeout=0),
             name='schema-swagger-ui'),
        path('redoc', schema_view.with_ui('redoc', cache_timeout=0),
             name='schema-redoc'),
]
