from rest_framework import serializers

from .models import Rate

class RateSerialiser(serializers.ModelSerializer):
    class Meta:
        model = Rate
        fields = '__all__'
