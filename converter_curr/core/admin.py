from django.contrib import admin

from .models import Rate


@admin.register(Rate)
class RateAdmin(admin.ModelAdmin):
    model = Rate
    list_display = ('base_curr', 'curr', 'value', 'created_at', 'updated_at')