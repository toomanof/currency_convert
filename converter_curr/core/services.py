import requests
from datetime import datetime, timedelta
from dataclasses import dataclass
from django.utils import timezone

from .models import Rate
from .serializers import RateSerialiser
from .utils import wrapper_response


@dataclass
class Rates:
    url_source = 'https://api.exchangeratesapi.io'
    prefix_latest = 'latest'
    base_currency = 'RUB'

    def _request(self, prefix, *params):
        params_url = f'?{"&".join(params)}' if params else ''
        return requests.get(f'{self.url_source}/'
                            f'{prefix}'
                            f'{params_url}')

    def set_base_currency(self, val):
        self.base_currency = val
        return True

    def get_current_rates(self, base_curr=None, *currency):
        if base_curr:
            self.set_base_currency(base_curr)

        currency_ulr = f"&symbols={','.join(currency)}"
        response = self._request(self.prefix_latest,
                                 f'base={self.base_currency}',
                                 f'{currency_ulr}')
        return response.json()

    def get_current_rates_db(self):
        dt_now = timezone.now()
        dt_start = datetime(dt_now.year, dt_now.month, dt_now.day)
        dt_fished = dt_start + timedelta(hours=24)
        return Rate.objects.filter(updated_at__gte=dt_start,
                                   updated_at__lt=dt_fished).values(
            'base_curr', 'curr', 'value'
        ).distinct()

    def save_to_base_current_rates(self):
        try:
            rates = self.get_current_rates('USD', 'CZK', 'PLN', 'USD', 'EUR')

            if 'rates' in rates and 'base' in rates:
                for currency, val in rates['rates'].items():
                    Rate.objects.create(base_curr=rates['base'],
                                       curr=currency,
                                       value=val)
        except Exception as err:
            raise Exception(err)
        else:
            return 'Rates is saved!'


class ResponseRates(Rates):
    @wrapper_response
    def request_current_rates_ecb(self, base_curr=None, *currency):
        return self.get_current_rates(base_curr, *currency)

    @wrapper_response
    def save_to_base_current_rates(self):
        return super().save_to_base_current_rates()

    @wrapper_response
    def request_current_rates_db(self):
        return RateSerialiser(
            self.get_current_rates_db(),
            many=True).data
