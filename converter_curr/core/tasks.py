from celery import Celery
from celery.schedules import crontab

from .services import Rates
app = Celery()


crontab(minute=0, hour=0)
def set_new_rates():
    Rates().save_to_base_current_rates()
