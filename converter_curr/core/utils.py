from django.core.exceptions import ObjectDoesNotExist, EmptyResultSet
from rest_framework import status
from rest_framework.response import Response


def wrapper_response(method):
    """
    Декоратор в случае совершения ошибки в обёрнутой функции
    возрашает msg ошибки.
    При удачном выполненнии функции возращает её ответ
    """
    def wrapper(self, *args, **kwargs):
        try:
            new_obj = method(self, *args, **kwargs)
        except Exception as e:
            json_ret = {
                'message': str(e),
                'code': 400001,
                'status': 'ERROR'}

            if isinstance(e, EmptyResultSet):
                return Response(
                    json_ret, status=status.HTTP_403_FORBIDDEN)
            if isinstance(e, ObjectDoesNotExist):
                return Response(
                    json_ret, status=status.HTTP_404_NOT_FOUND)
            return Response(json_ret,
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            if not isinstance(new_obj, Response):
                return Response(new_obj)
            return new_obj
    return wrapper